﻿namespace CocCocTestApp.Logic
{
    internal class Settings
    {
        /// <summary>
        /// Min count of dummy search results
        /// </summary>
        internal const int MinSuggestionCount = 3;

        /// <summary>
        /// Max count of dummy search results
        /// </summary>
        internal const int MaxSuggestionCount = 6;

        /// <summary>
        /// Lower time limit of dummy service search in ms
        /// </summary>
        internal const int MinDummySearchCompletionTime = 300;

        /// <summary>
        /// Upper time limit of dummy service search in ms
        /// </summary>
        internal const int MaxDummySearchCompletionTime = 300;

        /// <summary>
        /// Max time for retriving dummy strings
        /// </summary>
        internal const int SearchTimeout = 1000;
    }
}
