﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System.Collections.ObjectModel;
using CocCocTestApp.Logic.SuggestionProviders;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Threading;

namespace CocCocTestApp.Logic.ViewModels
{
    public class MainViewModel : ViewModelBase
    {
        #region Private fields

        private ObservableCollection<string> _searchResults;
        private IEnumerable<SuggestionProvider> _providers;
        private RelayCommand _searchCommand;
        private bool _isLoading;
        private string _query;

        #endregion

        public MainViewModel()
        {
            this._providers = ViewModelLocator.GetSuggestionProviders();
            this.SearchResults = new ObservableCollection<string>();
        }

        /// <summary>
        /// Search results collection
        /// </summary>
        public ObservableCollection<string> SearchResults
        {
            get { return _searchResults; }
            set
            {
                _searchResults = value;
                RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Loading indicator flag
        /// </summary>
        public bool IsLoading
        {
            get { return _isLoading; }
            set
            {
                _isLoading = value;
                RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Search query
        /// </summary>
        public string Query
        {
            get { return _query; }
            set
            {
                _query = value;
                RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Cancellation source for previos search. 
        /// We need it, if user types next symbol before previous seacrh finish.
        /// </summary>
        private CancellationTokenSource searchCancellationTokenSource;

        public RelayCommand SearchCommand
        {
            get
            {
                return _searchCommand = _searchCommand ?? new RelayCommand(async () =>
                    {
                        if (!string.IsNullOrEmpty(Query))
                        {
                            if (searchCancellationTokenSource != null)
                            {
                                searchCancellationTokenSource.Cancel();
                            }
                            searchCancellationTokenSource = new CancellationTokenSource();

                            await InvokeWithLoader(Search(searchCancellationTokenSource.Token));
                        }
                    });
            }
        }

        #region Private methods

        private Task Search(CancellationToken ct)
        {
            // clear search results
            this.SearchResults.Clear();

            var searchTasks = _providers
                .Select(async x =>
                {
                    //todo: pass cancellation token here too.
                    var results = await x.GetAsync(Query);
                    if(results != null)
                    {
                        // don't add results, if this request was cancelled
                        if (!ct.IsCancellationRequested)
                        {
                            AddSearchResults(results);
                        }
                    }
                });

            return Task.WhenAll(searchTasks);
        }

        private void AddSearchResults(IEnumerable<string> results)
        {
            foreach (var item in results)
            {
                this.SearchResults.Add(item);
            }
        }

        private async Task InvokeWithLoader(Task task)
        {
            IsLoading = true;
            await task;
            IsLoading = false;
        }

        #endregion
    }
}
