﻿using CocCocTestApp.Logic.SuggestionProviders;
using GalaSoft.MvvmLight.Ioc;
using Microsoft.Practices.ServiceLocation;
using System.Collections.Generic;

namespace CocCocTestApp.Logic.ViewModels
{
    public class ViewModelLocator
    {
        public ViewModelLocator()
        {
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);

            // suggestion providers
            RegisterSuggestionProvider<PhotoSuggestionProvider>();
            RegisterSuggestionProvider<ContactsSuggestionProvider>();
            RegisterSuggestionProvider<OneDriveSuggestionProvider>();
            RegisterSuggestionProvider<LocalDriveSuggestionProvider>();
            RegisterSuggestionProvider<InternetSearchSuggestionProvider>();

            // models
            SimpleIoc.Default.Register<MainViewModel>();
        }

        private void RegisterSuggestionProvider<T>()
            where T : SuggestionProvider
        {
            SimpleIoc.Default.Register<T>();
            SimpleIoc.Default.Register<SuggestionProvider>(() => SimpleIoc.Default.GetInstance<T>(), typeof(T).Name);
        }

        private MainViewModel _mainModel;
        public MainViewModel MainModel
        {
            get
            {
                return _mainModel = _mainModel ?? SimpleIoc.Default.GetInstance<MainViewModel>();
            }
        }

        public static IEnumerable<SuggestionProvider> GetSuggestionProviders()
        {
            return SimpleIoc.Default.GetAllInstances<SuggestionProvider>();
        }
    }
}
