﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CocCocTestApp.Logic.SuggestionProviders
{
    public abstract class SuggestionProvider
    {
        private Random random = new Random();

        public virtual async Task<IEnumerable<string>> GetAsync(string query)
        {
            // set cancellation time for search operation
            CancellationTokenSource cts = new CancellationTokenSource(Settings.SearchTimeout);
            cts.CancelAfter(Settings.SearchTimeout);

            var count = random.Next(Settings.MinSuggestionCount, Settings.MaxSuggestionCount + 1);
            try
            {
                // Set random task delay value
                // Ref: where “delay” is random delay from 300 to 2000 milliseconds
                var delay = GetRandomDelay();
                await Task.Delay(delay, cts.Token);

                // Generate random results. 
                // Ref: Returns list of dummy strings (3-6 in each) in random order.
                return GetDummyResults(count, query);
            }
            catch (OperationCanceledException ex)
            {
                return null;
            }
            catch (Exception ex)
            {
                Debug.Write(string.Format("Internal error. Exception: {0}", ex.Message));
                throw ex;
            }
        }

        protected virtual int GetRandomDelay()
        {
            return random.Next(Settings.MinDummySearchCompletionTime, Settings.MaxDummySearchCompletionTime);
        }

        /// <summary>
        /// Returns mock enumeration of strings
        /// </summary>
        /// <returns>Random size enumeration of random ordered strings</returns>
        private IEnumerable<string> GetDummyResults(int count, string query)
        {
            var suggestionProviderName = this.GetType().Name;
            return Enumerable
                .Range(0, count)
                .Select(x => string.Format("{0} result number {1} for request {2}", suggestionProviderName, x, query))
                .OrderBy(x => random.Next())
                .ToList();
        }
    }
}
